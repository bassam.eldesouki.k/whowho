var express = require('express');
const { Mongoose, Model } = require('mongoose');
const { findById } = require('../model/card');
const Card = require('../model/card');
const Round = require('../model/round');
var router = express.Router();

router.post('/api/cards', function(req, res, next){
    var card = new Card(req.body);
    card.save(function(err, card) {
        if (err) {return next(err); }
        if (card == null) {return res.status(404).json({"Message": "Card not found"});}
        res.status(201).json(card);
    });
});

router.delete('/api/cards/:id', function(req, res,next){
    var id =req.params.id;
    Card.findByIdAndDelete(id, function(err, card){
        if (err) {return next(err);}
        if (card == null) {return res.status(404).json({"Message": "Card not found"});}
        res.status(200).json({"Message": "Card deleted", card});
    });
});

router.get('/api/cards/:id', function(req, res, next){
    const cardId = req.params.id
    Card.findById(cardId, function(err, card){
        if(err){return next(err);}
        if(card==null){return res.status(404).json({'Message': 'Card not found'});}
        res.status(200).json(card);
    });
});

router.get('/api/cards', function(req,res,next){
    Card.find(function(err, card){
        if(err){return next(err);}
        if(card==null){return res.status(404).json({'Message': 'Card not found'});}
        res.status(200).json(card);
    });
}); 

router.get('/api/cards/categories/:category_name', function(req, res, next){
    var category_names = req.params.category_name; 
    var category_filter = { categories: { $in: category_names} }; 
    Card.find(category_filter, function (err,cards){ 
        if (err) { return next(err);}
        if (cards === null) {return res.status(404).json({"Message": `no card with ${category_name} category found`});}
        res.status(200).json(cards);
    });
});

router.put('/api/cards/:id', function(req, res, next){
    var id =req.params.id;
    Card.findById(id, function(err, card){
        if (err) {return next(err);}
        if (card == null) {return res.status(404).json({"Message": "Card not found"});}
        card.categories = req.body.categories;
        card.content = req.body.content;
        card.save();
        res.status(200).json(card);
    });
});

module.exports = router;