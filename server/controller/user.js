var express = require('express');
var router = express.Router();
var User = require('../model/user');
var Game = require('../model/game');
var mongoose = require('mongoose');

router.post('/api/users', function(req, res, next){
    var user = new User(req.body);
    user.save(function(err, user) {
        if (err) {return next(err); }
        if(user==null){return res.status(404).json({'Message': 'User not found'});}
        res.status(201).json(user);
    });
});

router.get('/api/users/:id', function(req, res, next){
    var id = req.params.id;
    User.findById(id, function(err, user){
        if(err){return next(err);}
        if(user==null){return res.status(404).json({'Message': 'User not found'});}
        res.status(200).json(user);
    });
});

router.get('/api/users', function(req, res, next){
    User.find(function(err, user){
        if(err){return next(err);}
        if(user==null){return res.status(404).json({'Message': 'User not found'});}
        res.status(200).json(user);
    });
});

router.get('/api/users/:id/games', function(req, res, next){ 
    var user_id = req.params.id;
    User
        .find()
        .where('_id').equals(user_id)
        .populate('games').sort({gameName:'asc'})
        .exec(function(err, user){
            if(err) {return next(err);}
            if(user==null){return res.status(404).json({'Message': 'User not found'});}
            res.status(200).json(user[0]);
        });
});

router.put('/api/users/:id', function(req, res, next){
    var id =req.params.id;
    User.findById(id, function(err, user){
        if (err) {return next(err);}
        if (user == null) {return res.status(200).json({'Message': 'User not found'});}
        user.username = req.body.username;
        user.games = req.body.games;
        user.save();
        res.status(200).json(user);
    });
}); 

router.patch('/api/users/:user_id', function(req,res,next){
    const userID = mongoose.Types.ObjectId (req.params.user_id);
    const games = req.body.games;
    User.findOne({_id:userID},function(err,user){
        if (err) {return next(err); }
        if (user == null) {return res.status(404).json({'Message': 'User not found'});}
        if (games != null){
            for (let index = 0; index < games.length; index++) {
                if (user.games.indexOf(games[index]) === -1) user.games.push(games[index]);
            }
        }
        user.save(function(err, user) {
            if (err) { return next(err);}
            if (user == null) {return res.status(404).json({'Message': 'User not found'});}
            res.status(200).json({'message':'these games have been added to the user', user});
        });
    });
});

router.delete('/api/users/:id/games/:game_id', function(req, res, next){
    const game_id = req.params.game_id;
    const user_id = req.params.id;
    User.findById(user_id,function(err,user){ 
        if (err) {return next(err); }
        if (user == null) {return res.status(200).json({'Message': 'User not found'});}
        const index = user.games.findIndex(game => game.toString() == game_id.toString());
        user.games.splice(index, 1);
        user.save(function(err, user) {
            if (err) { return next(err);}
            res.status(200).json({'Message': 'Game removed from this user', user}); 
        });
    }); 
});
 
router.delete('/api/users/:id', function(req, res, next){
    var id = req.params.id;
    User.findByIdAndDelete(id, function(err, user){
        if (err) {return next(err);}
        if (user == null){return res.status(404).json({'Message': 'User not found'});}
        res.status(200).json({'Message': 'User deleted', user});
    });
});

router.delete('/api/users/', function(req, res, next){
    User.deleteMany(function(err, users){
        if (err) {return next(err);}
        if (users == null) {return res.status(404).json({'Message': 'users not found'});}
        res.status(200).json({'Message': 'All users deleted', users});
    });
});

router.get('/api/users/names/:username', function(req, res, next){
    var Ausername = req.params.username; 
    var user_filter = { username: Ausername }; 
    User.find(user_filter, function (err,user){ 
        if (err) { return next(err);}
        if (user <= 0) {return res.status(200).json(user[0]);}
        res.status(200).json(user[0]);
    });
});

module.exports = router;