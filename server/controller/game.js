var express = require('express');
const mongoose = require('mongoose');
const { findById } = require('../model/game');
const Game = require('../model/game');
var router = express.Router();

router.post('/api/games', function(req, res, next){
    var game = new Game(req.body);
    game.save(function(err, game) {
        if (err) { return next(err);}
        if (game == null) {return res.status(404).json({'Message': 'Game not found'});}
        res.status(201).json(game);
    });
});

router.delete('/api/games/:id', function(req, res, next){
    var id =req.params.id;
    Game.findByIdAndDelete(id, function(err, game){
        if (err) {return next(err);}
        if (game == null) {return res.status(404).json({'Message': 'Game not found'});}
        res.status(200).json({'Message': 'Game deleted', game});
    });
});

router.delete('/api/games', function(req, res, next){
    Game.remove({}, function(err, games){
        if(err){return next(err);}
        if (games == null) {return res.status(200).json({'Message': 'No games found'});}
        res.status(200).json(games);
    });
});

router.get('/api/games/:id', function(req, res, next){
    var id =req.params.id;
    Game.findById(id,function(err, game){
        if (err) {return next(err);}
        if (game == null) {return res.status(404).json({'Message': 'Game not found'});}
        res.status(200).json(game);
    });
});

router.get('/api/games', function(req, res, next){
    Game.find(function(err, game){
        if(err){return next(err);}
        if(game==null){return res.status(404).json({'Message': 'Game not found'});}
        res.status(200).json(game);
    });
}); 

router.put('/api/games/:id', function(req, res, next){
    var id =req.params.id;
    Game.findById(id, function(err, game){
        if (err) {return next(err);}
        if (game == null) {return res.status(404).json({'Message': 'Game not found'});}
        game.user = req.body.user;
        game.gameName = req.body.gameName;
        game.rounds = req.body.rounds;
        game.categories = req.body.categories;
        game.save(function(err, game) {
            if (err) { return next(err);}
            if (game == null) {return res.status(404).json({'Message': 'game not found'});}
            res.status(200).json({game});
        });
    });
});

router.patch('/api/games/:game_id', function(req,res,next){
    const game_id = mongoose.Types.ObjectId (req.params.game_id);
    const rounds = req.body.rounds;

    Game.findOne({_id:game_id},function(err,game){
        if (err) {return next(err); }
        if (game == null) {return res.status(404).json({'Message': 'game not found'});}
        if (rounds != null){
            for (let index = 0; index < rounds.length; index++) {
                if (game.rounds.indexOf(rounds[index]) === -1) game.rounds.push(rounds[index]);
            }
        }
        game.save(function(err, game) {
            if (err) { return next(err);}
            if (game == null) {return res.status(404).json({'Message': 'game not found'});}
            res.status(200).json({'message':'Game updated', game});
        });
    });
});

router.delete('/api/games', function(req, res, next){
    Game.deleteMany(function(err, games){
        if (err) {return next(err);}
        if (games == null) {return res.status(404).json({'Message': 'Game not found'});}
        res.status(200).json({'Message': 'All Games deleted', games});
    });
});

module.exports = router;