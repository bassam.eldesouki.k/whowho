var express = require('express');
const mongoose = require('mongoose');
const { findById } = require('../model/round');
const round = require('../model/round');
const router = express.Router();
const Round = require('../model/round');
const Card = require('../model/card');
const { json } = require('body-parser');

router.delete('/api/rounds/:round_id/cards/:card_id', function(req,res,next){
    const round_id = req.params.round_id;
    const card_id = req.params.card_id;
    
    Round.findById(round_id,function(err,round){ 
        if (err) {return next(err); }
        if (round == null) {return res.status(200).json({'Message': 'round not found'});}

        const index = round.cards.findIndex(round => round.toString() == card_id.toString());
        round.cards.splice(index, 1);
        round.save(function(err, round) {
            if (err) { return next(err);}
            res.status(200).json({'Message': 'Card removed from this round', round}); 
        });
    }); 
});

router.get('/api/rounds/:round_id/cards/:card_id', function(req, res,next){
    const round_id =req.params.round_id;
    const card_id =req.params.card_id;
    Round.findById(round_id, function(err, round){
        if (err) {return next(err);}
        if (round == null) {return res.status(404).json({'Message': 'Round not found'});}
        Card.findById(card_id,function(err,card){
            if (err) {return next(err);}
            res.status(200).json(card);
        });
    });
});

router.patch('/api/rounds/:round_id', function(req,res,next){
    const round_id = mongoose.Types.ObjectId (req.params.round_id);
    const cards = req.body.cards;
    Round.findOne({_id:round_id},function(err,round){
        if (err) {return next(err); }
        if (round == null) {return res.status(404).json({'Message': 'round not found'});}
        if (cards != null){
            for (let index = 0; index < cards.length; index++) {
                if (round.cards.indexOf(cards[index]) === -1) round.cards.push(cards[index]); 
            }
        }
        round.save(function(err, round) {
            if (err) { return next(err);}
            res.status(200).json({'message':'these cards have been added to the round', round});
        });
    });
});

router.post('/api/rounds/cards/category/:names', function(req, res,next){
    const category_names = req.params.names.split(','); 
    const category_filter = { categories: { $in: category_names} }; 
    const round = new Round(req.body);
    round.save(function(err, round){

        if (err) { return next(err);}
        if (round == null) {return res.status(404).json({'Message': 'Round not found'});}
    
        Card.find(category_filter,function(err,cards){ 
            if (err) { return next(err);}
            if (cards === null) {
                return res.status(404).json({'Message': `no card with ${category_names} category found`});
            }
            const randomCards = []; 
            let random = 0;
            while (randomCards.length < 5){
                do {
                    random = Math.floor(Math.random() * cards.length); 
                }while (random > cards.length);
                if(randomCards.indexOf(cards[random]) !== -1){ 
                    continue;
                }
                randomCards.push(cards[random]);
            }
            round.cards = randomCards;
            round.save(function(err, round) {
                if (err) { return next(err);}
                res.status(201).json(round);
            }); 
        });
    });
});

router.post('/api/rounds', function(req, res,next){
    const round = new Round(req.body);
    round.save(function(err, round) {
        if (err) { return next(err);}
        if (round == null) {return res.status(404).json({'Message': 'Round not found'});}
        res.status(201).json(round);
    });
});

router.get('/api/rounds', function(req, res,next){
    Round.find().populate('cards').exec(function(err, rounds){
        if (err) {return next(err);}
        if (rounds == null) {return res.status(404).json({'Message': 'Round not found'});}
        res.status(200).json({'message':'All rounds',rounds});
    });
});

router.get('/api/rounds/:id', function(req, res,next){
    const id =req.params.id;
    Round.findById(id).populate('cards').exec(function(err, round){
        if (err) {return next(err);}
        if (round == null) {return res.status(404).json({'Message': 'Round not found'});}
        res.status(200).json(round);
    });
});

router.put('/api/rounds/:id', function(req, res,next){
    const id =req.params.id;
    Round.findById(id).populate('cards').exec(function(err, round){
        if (err) {return next(err);}
        if (round == null) {return res.status(404).json({'Message': 'Round not found'});}
        round.cards = req.body.cards;
        round.save(function(err, round) {
            if (err) { return next(err);}
            res.status(200).json({'Message': 'Round Updated', round});
        });
    });
});

router.delete('/api/rounds/:id', function(req, res,next){
    const id =req.params.id;
    Round.findByIdAndDelete(id, function(err, round){
        if (err) {return next(err);}
        if (round == null) {return res.status(404).json({'Message': 'Round not found'});}
        res.status(200).json({'Message': 'Round deleted', round});
    }).populate('cards');
});

router.delete('/api/rounds', function(req, res, next){
    Round.deleteMany(function(err, rounds){
        if (err) {return next(err);}
        if (rounds == null) {return res.status(404).json({'Message': 'rounds not found'});}
        res.status(200).json({'Message': 'All rounds deleted', rounds});
    });
});

module.exports = router;
