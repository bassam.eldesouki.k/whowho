var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var gameSchema = new Schema({
    gameName: {type: String},
    rounds: [{type: mongoose.Schema.Types.ObjectId, ref: 'rounds'}],
    categories: {type: String}
});

module.exports = mongoose.model('games', gameSchema);