var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// creating a round schema 
var roundSchema = new Schema({
    cards: [{type: mongoose.Types.ObjectId, ref: 'cards'}]
});

// exporting the round schema
module.exports = mongoose.model('rounds', roundSchema);

