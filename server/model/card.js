var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//var random = require('mongoose-random'); // npm install mongoose-random 

var cardSchema = new Schema({
    content: {type: String},
    categories: [{type: String}]
});


module.exports = mongoose.model('cards', cardSchema);

