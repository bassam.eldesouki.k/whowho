var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var userSchema = new Schema({
    username: {type: String},
    games: [{type: mongoose.Schema.Types.ObjectId, ref: 'games'}],
});



module.exports = mongoose.model('users', userSchema);