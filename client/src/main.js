import Vue from 'vue'
import App from './App.vue'
import router from './router'
import { store } from './store/index.js'
import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(BootstrapVue)

Vue.config.productionTip = false

Vue.config.errorHandler = (err, vm, info) => {
  if (err) {
    const variant = 'danger'
    this.$bvToast.toast('Something went wrong', {
      title: `Variant ${variant || 'default'}`,
      variant: variant,
      solid: true
    })
  }
}

window.onerror = function (message, source, lineno, colno, error) {
  if (error) {
    const variant = 'danger'
    this.$bvToast.toast('Something went wrong', {
      title: `Variant ${variant || 'default'}`,
      variant: variant,
      solid: true
    })
    return false
  }
}

new Vue({
  router,
  store,
  render: function (h) { return h(App) }
}).$mount('#app')
