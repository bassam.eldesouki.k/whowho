import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/Home.vue'
import Game from './views/Game.vue'
import Card from './components/Card.vue'
import Prepare from './views/Prepare.vue'
Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/game',
      name: 'game',
      component: Game
    },
    {
      path: '/cards',
      name: 'card',
      component: Card
    },
    {
      path: '/prepare',
      name: 'prepare',
      component: Prepare
    }
  ]
})
